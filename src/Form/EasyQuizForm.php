<?php
/**
 * @file
 * Contains \Drupal\easy_quiz\Form\EasyQuizForm
 */
namespace Drupal\easy_quiz\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\Core\Url;


class EasyQuizForm extends FormBase{
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'resume_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state) {
        $node = \Drupal\node\Entity\Node::load(73);
        $answer_options = $node->get('field_answer_options');
        $answer_options_array = array();
        foreach ($answer_options as $option){
            $answer_options_array[] = $option->value;
        }

        $title = $node->get('title');
        $form['quiz_form'] = array (
            '#type' => 'radios',
            '#title' => $title->value,
            '#options' => $answer_options_array,
        );
        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
            '#type' => 'submit',
            '#value' => $this->t('Save'),
            '#button_type' => 'primary',
        );
        return $form;
    }

    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        // TODO
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {
        $user = \Drupal::currentUser();
        $my_answer = Node::create(['type' => 'user_answers']);
        $my_answer->set('title', 'My answer');
        $my_answer->set('field_answer_submitted', 'This is my answer');
        $my_answer->set('field_marks_obtained', 1);
        $my_answer->set('field_question', 73);
        $my_answer->set('field_right_or_wrong', 1);
        $my_answer->setOwnerId($user->id());
        $my_answer->enforceIsNew();
        $my_answer->save();

        $node = $node = \Drupal\node\Entity\Node::load($my_answer->id());

        \Drupal::messenger()->addMessage('Your answer submitted');
        $response = Url::fromUserInput('/node/'.$node->id());
        $form_state->setRedirectUrl($response);
    }
}
